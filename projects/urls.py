from django.urls import path
from projects.views import list_project_view, show_project, create_project_view

urlpatterns = [
    path("", list_project_view, name="list_projects"),
    path("<int:id>", show_project, name="show_project"),
    path("create/", create_project_view, name="create_project"),
]

from django.shortcuts import render, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from tasks.models import Task
from projects.forms import CreateProjectForm


# Create your views here.
@login_required
def list_project_view(request):
    projects = Project.objects.filter(owner=request.user)
    context = {"projects": projects}

    return render(request, "projects/list_projects.html", context)


@login_required
def show_project(request, id):
    projects = Project.objects.filter(owner=request.user)
    tasks = Task.objects.filter(id=id)
    context = {"projects": projects, "tasks": tasks}

    return render(request, "projects/show_project.html", context)


@login_required
def create_project_view(request):
    if request.method == "POST":
        form = CreateProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = CreateProjectForm()

    context = {"form": form}
    return render(request, "projects/create_project.html", context)
